#!/usr/bin/bash
cd /root/nginx
git pull
docker build -t swr.na-mexico-1.myhuaweicloud.com/gitlab/nginx:webhook .
docker push swr.na-mexico-1.myhuaweicloud.com/gitlab/nginx:webhook
kubectl set image deploy/nginx nginx=swr.na-mexico-1.myhuaweicloud.com/gitlab/nginx:webhook
kubectl rollout restart deploy/nginx
