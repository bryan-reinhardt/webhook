#!/usr/bin/env python3
import os
import json
import hmac
import hashlib

from flask import Flask, jsonify, request

API_SECRET = os.environ.get('API_SECRET', '').encode()
TOKEN_HEADER = os.environ.get('TOKEN_HEADER', 'X-Hub-Signature-256')
CONTROL_HOST = os.environ.get('CONTROL_HOST','localhost')

# initialize our Flask application
app = Flask(__name__)

def signature_is_valid(request):
    request_signature = request.headers[TOKEN_HEADER]
    message = request.data
    if TOKEN_HEADER == 'X-Hub-Signature-256':

        signature = hmac.new(
            API_SECRET,
            msg=message,
            digestmod=hashlib.sha256
        ).hexdigest()

        return request_signature == f'sha256={signature}'

    elif TOKEN_HEADER == 'X-Gitlab-Token':
        return request_signature == API_SECRET.decode()

@app.route("/execute", methods=["POST"])
def updateWeb():
    if request.method=='POST':
        try:
            if signature_is_valid(request):
                script_name = request.args.get('script', default = '*', type = str)
                os.system(f"ssh -o StrictHostKeyChecking=no root@{CONTROL_HOST} 'bash -s' < scripts/{script_name}")
                return jsonify({'message': 'Update successfull'})

            else:
                return jsonify({'message': 'Not Authorized'}), 403

        except:
            return jsonify({'message': 'Error while processing your request'}), 500

if __name__=='__main__':
   app.run(debug = True, host = '0.0.0.0', port = 8080)
